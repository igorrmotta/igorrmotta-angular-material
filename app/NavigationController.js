(function () {
    angular.module('navigation')
        .controller('NavigationController', NavigationController);

    function NavigationController(navigationService, $mdSidenav, $scope, $location) {
        var self = this;

        self.pages = navigationService.pages;
        self.pageSelected = self.pages[0];
        self.showPage = ShowPage;
        self.showMenu = ShowMenu;

        $scope.$on('$locationChangeSuccess', function (event, newUrl, oldUrl, newState, oldState) {
            var path = $location.path();
            var page = navigationService.findPageByUrl(path);
            console.log(page);
            self.pageSelected = page;
        });

        function ShowPage(page) {
            self.pageSelected = page;
        }

        function ShowMenu() {
            $mdSidenav('left').toggle();
        }
    }
})();

