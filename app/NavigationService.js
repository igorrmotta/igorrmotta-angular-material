'use strict';
angular.module('navigation', ['ngMaterial']);

angular.module('navigation').service('navigationService', NavigationService);

function NavigationService() {
    var pages = [
        {
            title: 'About',
            icon: 'svg/person.svg',
            url: '/about'
        },
        {
            title: 'Skills',
            icon: 'svg/pencil.svg',
            url: '/skills'
        },
        {
            title: 'Education',
            icon: 'svg/education.svg',
            url: '/education'
        },
        {
            title: 'Extra Qualifications & Courses',
            icon: 'svg/library.svg',
            url: '/courses'
        },
        {
            title: 'Work Experience & Projects',
            icon: 'svg/work.svg',
            url: '/experience'
        },
        {
            title: 'Contact',
            icon: 'svg/contact.svg',
            url: '/contact'
        }
    ];

    this.pages = pages;
    this.findPageByUrl = findPageByUrl;

    function findPageByUrl(url) {
        var pages = this.pages;
        for (var i = 0; i < pages.length; i++) {
            var page = pages[i];
            if (String(page.url) == String(url)) {
                return page;
            }
        }
        return null;
    }

    return this;
}
